###################### Import Necessary Packages, Libraries ###########################
import streamlit as st
import numpy as np
import functions
from PIL import Image
import os
import time

def load_home():
    st.markdown(unsafe_allow_html=True, body="<h3><b><font color=red>What is Pneumonia?</font></b></h3><br>")
    st.markdown(unsafe_allow_html=True, body="<p>It is an inflammatory condition of the lung affecting primarily the small air sacs known as alveoli.Symptoms typically include some combination of productive or dry cough, chest pain, fever and difficulty breathing. The severity of the condition is variable. It is usually caused by infection with viruses or bacteria and less commonly by other microorganisms, certain medications or conditions such as autoimmune diseases.Risk factors include cystic fibrosis, chronic obstructive pulmonary disease (COPD), asthma, diabetes, heart failure, a history of smoking, a poor ability to cough such as following a stroke and a weak immune system. Diagnosis is often based on symptoms and physical examination. The disease may be classified by where it was acquired, such as community- or hospital-acquired or healthcare-associated pneumonia.</p><br>")
    st.markdown(unsafe_allow_html=True, body="<h3><b><font color=red>What are we solving?</font></b></h3><br>")
    st.markdown(unsafe_allow_html=True, body="<p>We develop an algorithm that can detect pneumonia from chest X-rays at a level exceeding practicing radiologists. Chest X-rays are currently the best available method for diagnosing pneumonia, playing a crucial role in clinical care and epidemiological studies. Pneumonia is responsible for more than 1 million hospitalizations and 50,000 deaths per year in the US alone.</p><br>")
    st.image(Image.open("xray_all.png"), use_column_width=True)
    st.markdown(unsafe_allow_html=True, body="<h3><b><font color=red>What does this Prototype Model Detect?</font></b></h3>")
    st.markdown(unsafe_allow_html=True, body="<p>It classifies chest X-Rays into 3 conditions:</p><ul><li>Normal</li><li>Viral pneumonia</li><li>Bacterial pneumonia</li> ")

###################### App Page ###########################
logo=Image.open('REALAI.jpg')
st.sidebar.image(logo, use_column_width=True)
st.sidebar.markdown(unsafe_allow_html=True, body="<h3><b>AI supported Pneumonia Detection App</b></h2>")  
st.markdown(unsafe_allow_html=True, body="<h1 align=center><font color=red><b>Curae PnuemoApp</b></font></h2>")
st.markdown(unsafe_allow_html=True, body="<h2 align=center><font color=Green><b>An interactive web-app prototype for PoC demonstration of Chest X-ray</b></font></h2>")


if st.sidebar.checkbox("About Curae PneumoApp"):
    load_home()
st.sidebar.markdown(unsafe_allow_html=True, body="<h3><b><font color=red>Prediction</font></b></h3>")
if st.sidebar.checkbox("Pneumonia Prediction - Chest-Xray"):
    st.markdown(unsafe_allow_html=True, body="<h3><b><font color=red>Select prediction option:</font></b></h3>")
    p_options = st.selectbox('Use Demo Data or Upload New Image',
        ('Select_option','Use_Demo_Data','Upload_New_Image'))

############### Trained Dataset Prediction ####################
    if p_options=="Use_Demo_Data":
        files = os.listdir('dataset/')
        d_options = st.selectbox('Select the option from the dataset:',
            ('Bacterial pneumonia', 'Viral pneumonia','No Pneumonia' ))

        i_options= st.selectbox('Select image from the prototype dataset:',
            files)
        img='dataset/'+i_options
        p_img = functions.preprocess_image(img)
        if st.checkbox('Zoom image'):
            image = np.array(Image.open(img))
            st.image(image, use_column_width=True)
        else:
            st.image(p_img)

        #####heatmap########
        #import matplotlib.pyplot as plt
        #import numpy as np
        #import cv2

        #image = cv2.imread(img, 0)
        #colormap = plt.get_cmap('inferno')
        #heatmap = (colormap(image) * 2**16).astype(np.uint16)[:,:,:3]
        #heatmap = cv2.cvtColor(heatmap, cv2.COLOR_RGB2BGR)

        #cv2.imshow('image', image)
        #cv2.imshow('heatmap', heatmap)
        #cv2.waitKey()

        # Loading model
        loading_msg = st.empty()
        loading_msg.text("Predicting...")
        model = functions.load_model()

        # Predicting result
        tic = time.perf_counter()
        prediction = functions.predict(model, p_img)
        toc = time.perf_counter()
        loading_msg.text('')
        st.text(f"Predicted the results in {toc - tic:0.4f} seconds")

        if d_options=='No Pneumonia':
            #st.progress(int(prediction[0][0] * 100))
            st.text(f"Probability that its a {d_options} is {round(prediction[0][0] * 100, 2)}%")
        elif d_options=='Bacterial pneumonia':
            #st.progress(int(prediction[0][1] * 100))
            st.text(f"Probability that its a {d_options} is {round(prediction[0][1] * 100, 2)}%")

        else:
            #st.progress(int(prediction[0][2] * 100))
            st.text(f"Probability that its a {d_options} is {round(prediction[0][2] * 100, 2)}%")
    
    ################ Uploaded Image Prediction #####################
    elif p_options=="Upload_New_Image":
        img = st.file_uploader(label="Upload X-Ray Chest image", type=['jpeg', 'jpg', 'png'], key="xray")
        if img is not None:
            p_img = functions.preprocess_image(img)
            if st.checkbox('Zoom image'):
                image = np.array(Image.open(img))
                st.image(image, use_column_width=True)
            else:
                st.image(p_img)

            # Loading model
            loading_msg = st.empty()
            loading_msg.text("Predicting...")
            model = functions.load_model()

            # Predicting result
            tic = time.perf_counter()
            prediction = functions.predict(model, p_img)
            toc = time.perf_counter()
            loading_msg.text('')
            st.text(f"Predicted the results in {toc - tic:0.4f} seconds")
            st.text(f"Probability that there's no Pneumonia is {round(prediction[0][0] * 100, 2)}%")
            st.text(f"Probability that there's Bacterial pneumonia is {round(prediction[0][1] * 100, 2)}%")
            st.text(f"Probability that there's Viral pneumonia is {round(prediction[0][2] * 100, 2)}%")